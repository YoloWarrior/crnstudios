﻿using System.Web;
using System.Web.Optimization;

namespace CrnStudios
{
	public class BundleConfig
	{
		// Дополнительные сведения об объединении см. на странице https://go.microsoft.com/fwlink/?LinkId=301862
		public static void RegisterBundles(BundleCollection bundles)
		{
			bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
						"~/Scripts/jquery-{version}.js"));

			bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
						"~/Scripts/jquery.validate*"));

			// Используйте версию Modernizr для разработчиков, чтобы учиться работать. Когда вы будете готовы перейти к работе,
			// готово к выпуску, используйте средство сборки по адресу https://modernizr.com, чтобы выбрать только необходимые тесты.
			bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
						"~/Scripts/modernizr-*"));

			bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
					   "~/Scripts/jquery-3.3.1.min.js",
                       "~/Scripts/jquery-migrate-3.0.1.min.js",
                         "~/Scripts/jquery-ui.js",
                         "~/Scripts/popper.min.js",
                        "~/Scripts/bootstrap.min.js",
						"~/Scripts/aos.js",
							 "~/Scripts/slick.min.js",
						 "~/Scripts/jquery.stellar.min.js",
						 "~/Scripts/owl.carousel.min.js",
                         "~/Scripts/jquery.stellar.min.js",
                        "~/Scripts/jquery.countdown.min.js",
                     "~/Scripts/jquery.magnific-popup.min.js",
                       "~/Scripts/bootstrap-datepicker.min.js",
						"~/Scripts/main.js"
					  ));

			bundles.Add(new StyleBundle("~/Content/css").Include(
					  "~/Content/bootstrap.css",
					   "~/Content/animate.css",
						  "~/Content/bootstrap.min.css",
                           "~/Content/magnific-popup.css",
                            "~/Content/jquery-ui.css",
                        "~/Content/owl.carousel.min.css",
                         "~/Content/owl.theme.default.min.css",
		                "~/Content/Site.css",
						 "~/Content/aos.css"

					  ));
		}
	}
}
