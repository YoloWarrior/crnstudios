using CrmStudios.Data;
using CrmStudios.Data.Models;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using System.Web;

[assembly: OwinStartup(typeof(CrnStudios.App_Data.Startup))]
namespace CrnStudios.App_Data
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // ����������� �������� � ��������
            app.CreatePerOwinContext<DataContext>(DataContext.Create);
            app.CreatePerOwinContext<UserManager>(UserManager.Create);
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Home"),
            });
        }
    }
}