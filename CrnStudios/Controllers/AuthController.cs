﻿using CrmStudios.Data.Models;
using CrnStudios.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CrnStudios.Controllers
{
	public class AuthController : Controller
	{
		private UserManager _userManager
		{
			get
			{
				return HttpContext.GetOwinContext().GetUserManager<UserManager>();
			}
		}

		private IAuthenticationManager AuthenticationManager
		{
			get
			{
				return HttpContext.GetOwinContext().Authentication;
			}
		}


		[HttpPost]
		public async Task<ActionResult> Login(string username,string password)
		{
			User user = await _userManager.FindAsync(username,password);

				if (user == null)
				{
					ModelState.AddModelError("", "Неверный логин или пароль.");
				}
				else
				{
					ClaimsIdentity claim = await _userManager.CreateIdentityAsync(user,
											DefaultAuthenticationTypes.ApplicationCookie);
					AuthenticationManager.SignOut();
					AuthenticationManager.SignIn(new AuthenticationProperties
					{
						IsPersistent = true
					}, claim);
					
				}

			return RedirectToAction("Index", "Home");
		}

		[HttpPost]
		public async Task<ActionResult> Register(UserViewModelForAuth model)
		{

			User user = new User
			{
				UserName = model.Username,
				UserInformation = new UserInformation
				{
					Balance = 1000,
					Name = model.Name,
				}
			};

			IdentityResult result = await _userManager.CreateAsync(user, model.Password);

			ClaimsIdentity claim = await _userManager.CreateIdentityAsync(user,
											DefaultAuthenticationTypes.ApplicationCookie);
			AuthenticationManager.SignOut();
			AuthenticationManager.SignIn(new AuthenticationProperties
			{
				IsPersistent = true
			}, claim);


			return RedirectToAction("Index", "Home");

		}
		[HttpGet]
		public ActionResult Logout()
		{
			AuthenticationManager.SignOut();
			return RedirectToAction("Index", "Home");
		}
	}
}