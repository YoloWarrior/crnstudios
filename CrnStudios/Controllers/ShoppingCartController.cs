﻿using CrmStudios.Data.Interfaces;
using CrmStudios.Data.Models;
using CrmStudios.Data.Repositories;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CrnStudios.Controllers
{
	public class ShoppingCartController : Controller
	{
		IReposWrapper _repos;

		public ShoppingCartController()
		{
			_repos = new RepositoryWrapper();
		}

		[HttpPost]
		public ActionResult AddProduct(string productName, float price)
		{
			var product = _repos.Product.Get(x => x.Name == productName);
			var shoppingCarts = _repos.ShoppingCart.GetAll().ToList();

			var shoppingCart = new ShoppingCart
			{
				UserId = User.Identity.GetUserId(),
				ProductId = product.Id,
				ProductCost = price,
				ShoppingCartId = shoppingCarts.Count + 1
			};
			_repos.ShoppingCart.Create(shoppingCart);

			return RedirectToAction("Index", "Home");
		}

		[HttpPost]
		public PartialViewResult DeleteProductFromShoppingCart(int Id)
		{
			var user = _repos.User.Get(x => x.Id == User.Identity.GetUserId());

			_repos.ShoppingCart.Delete(_repos.ShoppingCart.Get(x=>x.ShoppingCartId==Id));

			var products = GetProductsFromDB();

			return PartialView("~/Views/Shared/_ShoppingCart.cshtml", products);
		}

		[HttpPost]
		public ActionResult BuyProducts(float price)
		{

			var user = _repos.User.Get(x => x.Id == User.Identity.GetUserId());
			var shoppingCart = _repos.ShoppingCart.GetAllWithInclude(x => x.Product).ToList();
			shoppingCart = shoppingCart.Where(x => x.UserId == User.Identity.GetUserId()).ToList();

			foreach (var cart in shoppingCart)
			{
				_repos.ShoppingCart.Delete(cart);
			}

			user.UserInformation.Balance -= price;

			_repos.UserInformation.Edit(user.UserInformation);

			return RedirectToAction("Index", "Home");
		}

		public PartialViewResult GetProducts()
		{
			var products = GetProductsFromDB();

			return PartialView("~/Views/Shared/_ShoppingCart.cshtml", products);
		}

		private IEnumerable<ProductViewModel> GetProductsFromDB()
		{
			var products = new List<ProductViewModel>();

			var shoppingCart = _repos.ShoppingCart.GetAllWithInclude(x => x.Product).ToList();
			shoppingCart = shoppingCart.Where(x => x.UserId == User.Identity.GetUserId()).ToList();

			if (shoppingCart.Count > 0)
			{
				foreach (var shop in shoppingCart)
				{
					products.Add(new ProductViewModel
					{
						ImageUrl = shop.Product.ImageUrl,
						Name = shop.Product.Name,
						ProductPrice = shop.Product.ProductPrice,
						ShoppingCartId = shop.ShoppingCartId,
						UnitOfCalculation = shop.Product.UnitOfCalculation,
						ProductSummaryPrice = shop.ProductCost
					});
				}
			}

			return products;
		}
	}
}