﻿using CrmStudios.Data.Interfaces;
using CrmStudios.Data.Models;
using CrmStudios.Data.Repositories;
using CrnStudios.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CrnStudios.Controllers
{
	public class HomeController : Controller
	{
		IReposWrapper _repos;
		private UserManager _userManager
		{
			get
			{
				return HttpContext.GetOwinContext().GetUserManager<UserManager>();
			}
		}

		public HomeController()
		{
			_repos = new RepositoryWrapper();
		}

		public async Task<ActionResult> Index()
		{
			if(_repos.Product.GetAll().ToList().Count == 0)
			{
				GenerateProducts();
			}

			var userViewModel = new UserViewModel();
			var user = _repos.User.Get(x => x.Id == User.Identity.GetUserId());

			if (user != null)
			{
				userViewModel.isLogged = true;
				userViewModel.Name = user.UserInformation.Name;
				userViewModel.Balance = user.UserInformation.Balance;
				userViewModel.Orders = _repos.ShoppingCart.GetAll().ToList().Where(x => x.UserId == user.Id).Count();
			};
				      
			return View(userViewModel);
		}

		[HttpGet]
		[Route("ShoppingCart")]
		public ActionResult ShoppingCart()
		{
			return View("~/Views/Home/ShoppingCart.cshtml");
		}


		private void GenerateProducts()
		{
			var products = new List<Product>();

			products.Add(new Product
			{
				Name = "Мясо",
				ImageUrl = "meat.jpg",
				ProductPrice = 7.37F,
				UnitOfCalculation = "кг"
			});

			products.Add(new Product
			{
				Name = "Молоко",
				ImageUrl = "milk.jpg",
				ProductPrice = 1.35F,
				UnitOfCalculation = "л"
			});

			products.Add(new Product
			{
				Name = "Картофель",
				ImageUrl = "potato.jpg",
				ProductPrice = 0.70F,
				UnitOfCalculation = "кг"
			});

			products.Add(new Product
			{
				Name = "Овес",
				ImageUrl = "oat.jpg",
				ProductPrice = 390,
				UnitOfCalculation = "т"
			});

			foreach(var prod in products)
			{
				_repos.Product.Create(prod);
			}
			
		}
	}
}