﻿using CrmStudios.Data.Interfaces;
using CrmStudios.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CrnStudios.Controllers
{
    public class ProductController : Controller
    { 
        IReposWrapper _repos;

        public ProductController()
        {
            _repos = new   RepositoryWrapper();
        }
        
        [Route("GetProducts")]
        public ActionResult GetProducts()
        {
            var products = new List<ProductViewModel>();
            var p = _repos.Product.GetAll();

            foreach (var product in p)
            {
                products.Add(new ProductViewModel
                {
                    Name = product.Name,
                    Id = product.Id,
                    Description = product.Description,
                    ImageUrl = product.ImageUrl,
                    ProductPrice = product.ProductPrice,
                    UnitOfCalculation = product.UnitOfCalculation
                });
            }
            return PartialView("~/Views/Shared/_Products.cshtml",products);
        }
    }
}