﻿

namespace CrnStudios
{
    public class ProductViewModel
    {
		public int Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public float ProductPrice { get; set; }
		public string UnitOfCalculation { get; set; }
		public string ImageUrl { get; set; }
		public int ShoppingCartId { get; set; }
		public float ProductSummaryPrice { get; set; }
	}
}