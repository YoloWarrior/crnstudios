﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrnStudios.Models
{
	public class UserViewModel
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Surname { get; set; }
		public float Balance { get; set; }
		public int Orders { get; set; }
		public bool isLogged { get; set; }
	}

	public class UserViewModelForAuth
	{
		public string Username { get; set; }
		public string Name { get; set; }
		public string Password { get; set; }
	}
}