﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agricultural.Services.Enums
{
    [AttributeUsage(AttributeTargets.Field)]
    public class ReprAttribute : Attribute
    {
        public string Representation;
        public ReprAttribute(string representation)
        {
            this.Representation = representation;
        }
        public override string ToString()
        {
            return this.Representation;
        }
    }

    public enum UnitOfCalculation
	{
        [Repr("L")]
		Liter,
        [Repr("T")]
        Tonne
	}
}
