﻿using CrmStudios.Data.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrmStudios.Data
{
	public class DataContext : IdentityDbContext<User>
	{
		public DataContext():base("Server=(localdb)\\mssqllocaldb;Database=CrmStudios;Trusted_Connection=True")  {
			
		}
		public static DataContext Create()
		{
			return new DataContext();
		}
		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Entity<ShoppingCart>().HasKey(sc => new { sc.ShoppingCartId,sc.ProductId, sc.UserId });
			modelBuilder.Entity<UserInformation>().HasKey(x => x.UserInformationId);
			modelBuilder.Entity<Product>();
			modelBuilder.Entity<User>().ToTable("User");
			modelBuilder.Entity<IdentityRole>().ToTable("Role");
			modelBuilder.Entity<IdentityUserRole>().HasKey(x=>new {x.RoleId,x.UserId });
			modelBuilder.Entity<IdentityUserLogin>().HasKey(x => new {x.UserId });
			modelBuilder.Entity<User>()
							.HasRequired(x => x.UserInformation);

		}
        public DbSet<UserInformation>UserInformation { get; set; }

		public DbSet<Product> Products { get; set; }
		public DbSet<ShoppingCart> ShoppingCarts { get; set; }
	
	}
}
