﻿namespace CrmStudios.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ShoppingCartLast : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.ShoppingCarts");
            AddPrimaryKey("dbo.ShoppingCarts", new[] { "ShoppingCartId", "ProductId", "UserId" });
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.ShoppingCarts");
            AddPrimaryKey("dbo.ShoppingCarts", new[] { "ProductId", "UserId" });
        }
    }
}
