﻿using CrmStudios.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrmStudios.Data.Interfaces.InterfacesForEntity
{
	public interface IUserInformation : IGenereticRepos<UserInformation>
	{
	}
}
