﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CrmStudios.Data.Interfaces
{
	public interface IGenereticRepos<T> where T:class
	{
		IEnumerable<T> GetAll();
		void Edit(T entity);
		void Delete(T entity);
		IEnumerable<T> GetAllWithInclude(params Expression<Func<T, object>>[] includeProperties);
		T Get(Func<T, bool> predicate);
		void Create(T entity);
	}
}
