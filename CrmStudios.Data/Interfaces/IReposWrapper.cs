﻿using CrmStudios.Data.Interfaces.InterfacesForEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrmStudios.Data.Interfaces
{
	public interface IReposWrapper
	{
		IProduct Product { get; }
		IUser User { get; }
		IShoppingCart ShoppingCart { get; }

		IUserInformation UserInformation { get; }

	}
}
