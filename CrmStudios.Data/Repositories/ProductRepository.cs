﻿using CrmStudios.Data.Interfaces.InterfacesForEntity;
using CrmStudios.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrmStudios.Data.Repositories
{
	public class ProductRepository:GenereticRepository<Product>,IProduct
	{
		public ProductRepository(DataContext context) : base(context) { }
	}
}
