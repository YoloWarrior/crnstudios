﻿using CrmStudios.Data.Interfaces;
using CrmStudios.Data.Interfaces.InterfacesForEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrmStudios.Data.Repositories
{
	public class RepositoryWrapper : IReposWrapper
	{
		private DataContext _db;
		private readonly IProduct _product;
		private readonly IShoppingCart _shoppingcart;
		private readonly IUser _user;
		private readonly IUserInformation _userInf;

		public RepositoryWrapper(DataContext context = null)
		{
			_db = context ?? new DataContext();
		}

		public IProduct Product => _product ?? new ProductRepository(_db);
		public IUser User => _user ?? new UserRepository(_db);
		public IShoppingCart ShoppingCart => _shoppingcart ?? new ShoppingCartRepository(_db);
		public IUserInformation UserInformation => _userInf ?? new UserInformationRepository(_db);
	}
}
