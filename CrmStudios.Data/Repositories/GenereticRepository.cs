﻿using CrmStudios.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CrmStudios.Data.Repositories
{
	public class GenereticRepository<T> : IGenereticRepos<T> where T : class, new()
	{

		private DataContext _context;
		private DbSet<T> db;

		public GenereticRepository(DataContext context)
		{
			_context = context;
			db = _context.Set<T>();
		}

		public void Create(T entity)
		{
			db.Add(entity);
			_context.SaveChanges();
		}

		public T Get(Func<T, bool> predicate)
		{
			return db.AsNoTracking().SingleOrDefault(predicate);
		}

		public IEnumerable<T> GetAll()
		{
			return db;
		}
		 public void Edit(T entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
            _context.SaveChanges();
        }

		public void Delete(T entity)
		{
			db.Attach(entity);
			_context.Entry(entity).State = EntityState.Deleted;
			_context.SaveChanges();
		}

		public IEnumerable<T> GetAllWithInclude(params Expression<Func<T, object>>[] includeProperties)
		{
			return Include(includeProperties).ToList();
		}
		private IQueryable<T> Include(params Expression<Func<T, object>>[] includeProperties)
		{
			var query = db.AsQueryable();
			foreach (var include in includeProperties)
				query = query.Include(include);
			return query;
		}

	}
}
