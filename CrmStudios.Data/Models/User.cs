﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrmStudios.Data.Models
{
	public class User: IdentityUser
	{
		public virtual UserInformation UserInformation { get; set; }
	}
}
