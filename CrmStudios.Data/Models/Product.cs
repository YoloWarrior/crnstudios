﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrmStudios.Data.Models
{
	public class Product
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public float ProductPrice { get; set; }
		public string UnitOfCalculation { get; set; }
		public string ImageUrl { get; set; }
	}
}
