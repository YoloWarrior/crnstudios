﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrmStudios.Data.Models
{
public	class ShoppingCart
	{
		[Key]
		public int ShoppingCartId { get; set; }
		public string UserId { get; set; }
		public User User { get; set; }
		public int ProductId { get; set; }
		public Product Product { get; set; }

		public float ProductCost { get; set; }
	}
}
