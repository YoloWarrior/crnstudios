﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrmStudios.Data.Models
{
	public class UserInformation
	{
		public int UserInformationId { get; set; }
		public string Name { get; set; }
		public string Surname { get; set; }
		public float Balance { get; set; }

		public virtual User User { get; set; }
	}
}
